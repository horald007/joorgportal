<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Joorgportal</title>
<pre>
<h1>*** Joorgportal ***</h1>

<p>Joorgportal ist eine Groupware für Familien und Privatpersonen.</p>

License: <a href="LICENSE.txt">GNU GPL v3.0</a>

<h3>Install</h3>
You may install by copying the App on your webspace and start index.php.

<h3>System Requirements</h3>
You need PHP >= 5.3.0. and MySQL driver.
This version is compatible with <b>PHP 7</b>.  

<h1>Open-Source-Programmierer zur Projektunterstützung gesucht. Kontakt </span><a href='http://sourceforge.net/p/joorgportal/discussion/german/thread/dd6c74a6/' target='_blank'>hier</a>.</h1>
</pre>
