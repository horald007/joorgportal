<?php
$listarray = array ( 
                     array ( 'label' => 'Aufgabe',
                             'name' => 'aufgabe',
                             'width' => 50, 
                             'type' => 'selectid',
                             'dbtable' => 'tblbelohnaufgaben',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_aufgabe' ),
                     array ( 'label' => 'Benutzer',
                             'name' => 'benutzer',
                             'width' => 50, 
                             'type' => 'selectid',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'seldbwhere' => "fldbelohn='J'",
                             'dbfield' => 'fldid_user' ),
                     array ( 'label' => 'Punkte',
                             'name' => 'pkt',
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldpunkte' ),
                     array ( 'label' => 'Startpunkte',
                             'name' => 'startpkt',
                             'default' => '0',
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldstartpkte' ));


$filterarray = array (
                     array ( 'label' => 'Benutzer:',
                             'name' => 'fltuser', 
                             'width' => 1, 
                             'type' => 'selectid',
                             'sign' => '=',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'selwhere' => "fldbelohn='J'",
                             'dbfield' => 'fldid_user' ));
                             
$pararray = array ( 'headline' => 'Belohnungspunkte',
                    'dbtable' => 'tblbelohnpunkte',
                    'orderby' => '',
                    'strwhere' => '',
                    'fldindex' => 'fldindex');
                    
?>