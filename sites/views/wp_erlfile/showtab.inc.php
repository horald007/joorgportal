<?php
$listarray = array ( array ( 'label' => 'Bezeichnung',
                             'name' => 'bez', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Typ',
                             'name' => 'typ', 
                             'default' => 'FILE',
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldtyp' ));

$pararray = array ( 'headline' => 'Erledigungsgruppe',
                    'dbtable' => 'tblgrperl',
                    'orderby' => 'fldbez',
                    'strwhere' => 'fldtyp="FILE"',
                    'fldindex' => 'fldindex');
?>