<?php
$listarray = array ( array ( 'label' => 'Bezeichnung',
                             'name' => 'bez',
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Typ',
                             'name' => 'typ',
                             'width' => 10, 
                             'type' => 'text',
                             'dbfield' => 'fldtyp' ));                                                 

$pararray = array ( 'headline' => 'Darstellungsform',
                    'dbtable' => 'tbldarform',
                    'orderby' => '',
                    'strwhere' => '',
                    'fldindex' => 'fldindex');
?>