<?php
include("config.php");
include("menu.php");
include("classes/checkupgrade.php");

function showhead() {
    echo "    <!DOCTYPE html>";
    echo "    <html>";
    echo "    <head>";
    echo "    <title>Joorgportal</title>";
    echo "    <!-- Bootstrap -->";
    echo "    <link href='includes/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>";
    echo "    <link href='includes/bootstrap/css/bootstrap.datepicker.css' rel='stylesheet' media='screen'>";
    echo "    <link href='includes/styles/body.css' rel='stylesheet' media='screen'>";
    echo "    </head>";
    echo "    <body>";
    echo "    <script src='includes/bootstrap/js/jquery-latest.js'></script>";
    echo "    <script src='includes/bootstrap/js/bootstrap.min.js'></script>";
    echo "    <script src='includes/bootstrap/js/bootstrap-datepicker.js'></script>";
    //getversion
    echo "<script src='includes/meinjs/getversion.js'></script>";
}

function showfoot() {
    echo "    </body>";
    echo "    </html>";
}

function startseite($gdbcon,$layout,$menuuser,$menuusername,$versnr) {
  $dbFile="config.php";
  if (filesize($dbFile) == 0 ) {
//    startinstall(); 
  } else {
    showhead();
    check_version($versnr);
    startmymenu($gdbcon,$menuuser,$menuusername,$versnr);
    showfoot();
  }
}

$versnr=$_POST['versnr'];
$layout="joorgportal";
startseite($gdbcon,$layout,$menuuser,$menuusername,$versnr);

?>