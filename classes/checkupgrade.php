<?php

function check_version($versnr) {
  $string = file_get_contents("version.json");
  $json_a = json_decode($string, true);
  $locvers=$json_a['versnr'];
  if ($versnr=="") {
    echo "<script>"; 
    echo "  indexvers_anfordern(); ";
    echo "</script>";
  }
//echo $locvers.",".$versnr."=versnr<br>";
  if ($locvers<$versnr) {
    echo "<div class='alert alert-info'>";
    echo "<a href='classes/checkupdate.php?getvers=".$versnr."'>Neue Version ".$versnr." verfügbar</a><br>";
    echo "</div>";
  }
}

?>