<?php
$pfad = urldecode($_GET['pfad']);
$pfad = str_replace("/var/www/html","http://localhost",$pfad);
$filename = $pfad.urldecode($_GET['filename']);
$pos = strpos($filename, '.', 1);
$len = strlen($filename);
$suffix = substr($filename,$pos+1,$len-$pos);
//echo $pos.",".$len."<br>";
//echo $suffix."=suffix<br>";
//echo $filename."<br>";
$end = substr($filename,$len-4,4);
if ($suffix=="mp4") {
  include("bootstrapfunc.php");
  bootstraphead();
  bootstrapbegin('Dateimanager');
  echo "<div class='alert alert-info'>";
  echo "Datei:".$filename;
  echo "</div>";
  echo "<video poster='".$filename."' width='640' height='400' controls>";
  echo "<source src='".$filename."' type='video/mp4' />";
  echo "  Ihr Browser kann dieses Video leider nicht wiedergeben.<br/>";
  echo "</video>";
  bootstrapend();
}
if ($suffix=="jpg" || $end=".jpg") {
  include("bootstrapfunc.php");
  bootstraphead();
  bootstrapbegin('Dateimanager');
  echo "<div class='alert alert-info'>";
  echo "Datei: ".$filename."<br>";
  echo "</div>";
  echo "<a href='showjpg.php?filename=".$filename."' class='btn btn-primary btn-sm active' role='button'>volle Größe</a><br><br>"; 
  echo "<img src='".$filename."' width='320' height='200' />";
  bootstrapend();
}
if ($suffix=="pdf") {
  header('Content-Type: application/pdf');
  $pdf = file_get_contents($filename);
  echo $pdf;
}
?>