<?php
include ("../config.php");
include("famkaleintragfunc.php");
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Familienkalender");
$userid=$_GET['userid'];
$datum=$_GET['datum'];
//echo $userid."<br>";
//echo $datum."<br>";
$famkal = $_GET['famkal'];
if (isset($_GET['actmon'])) {
  $actmon=$_GET['actmon'];
} else {	
  $actmon=date("m");
}
if ($famkal==1) {
  if (isset($_REQUEST['submit'])) { 
    $terminbez=$_POST['terminbez'];
    $ganzertag=$_POST['ganzertag'];
    echo $ganzertag."=ganzertag<br>";	
    echo "<div class='alert alert-success'>";
    echo "Termin wird gespeichert.<br>"; 
    echo "</div>";  
    famkalspeichern($gdbcon,$userid,$datum,$terminbez);
  } else {
    echo "<div class='alert alert-warning'>";
    echo "Vorgang abgebrochen.<br>"; 
    echo "</div>";  
  }  
  //echo "<meta http-equiv='refresh' content='0; URL=famkal.php?actmon=".$actmon."'>";
  echo "<a class='btn btn-primary' href='famkal.php?actmon=".$actmon."'>zurück</a> ";
} else {
  $terminbez=$_GET['bez'];
  famkaleingabe($userid,$datum,$terminbez,$actmon);
}  
bootstrapend();
?>