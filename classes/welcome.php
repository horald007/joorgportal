<?php
header("content-type: text/html; charset=utf-8");
include("../config.php");
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Welcome");

$string = file_get_contents("../version.json");
$json_a = json_decode($string, true);
$versnr=$json_a['versnr'];
$versdat=$json_a['versdat'];

$chkupdate="checkupdate.php";
echo "<a href='".$chkupdate."' class='btn btn-primary btn-sm active' role='button'>Update prüfen</a> ";
echo "<a href='help.php?pagename=welcome' class='btn btn-primary btn-sm active' role='button'>Hilfe</a> ";

echo "<pre>";
echo "<table>";
echo "<tr><td>Stand</td>  <td>: ".$versdat."</td></tr>";
echo "<tr><td>Version</td><td>: ".$versnr."</td></tr>";
echo "<tr><td>Autoinc-Start/dbsyncnr</td>  <td>: ".$autoinc_start."</td></tr>";
echo "<tr><td>Autoinc-Step</td>  <td>: ".$autoinc_step."</td></tr>";
echo "<tr><td>aktueller User</td>  <td>: ".get_current_user()."</td></tr>";
if (isset($_GET['ipaddr'])) {
  $ipaddr=$_GET['ipaddr'];
} else {
  $ipaddr=$_SERVER['REMOTE_ADDR'];
}
echo "<tr><td>aktueller IP Addr</td>  <td>: ".$ipaddr."</td></tr>";
//echo "<tr><td>aktueller Computername</td>  <td>: ";
//echo "<script>";
//echo "alert(location.hostname);";
//echo "</script>";
//echo "</td></tr>";
echo "<tr><td>aktueller Hostname</td>  <td>: ".gethostname()."</td></tr>";
echo "</table>";
echo "</pre>";

bootstrapend();
?>